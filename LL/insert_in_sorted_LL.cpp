#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void rev(Node *n);
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void printLL(Node *n){
  if(n == NULL)
    return;

  Node *c = n;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void sortedInsertion(Node* &h, int v){
  cout << "he";
  // Make a new node
  Node* newNode = new Node(v);

  if(h == NULL)
    h = newNode;
  else{
    Node *c = h;
    Node *p = NULL;

    // traverse until 'c' gets bigger than 'v'
    // loop will end either when c hits NULL or when c's value is bigger than v
    while(c->value < v && c != NULL){
      p = c;
      c = c->next;
    }


    //Case 3: if c reached NULL (last element of list is smaller than v)
    if(c == NULL){
      p->next = newNode; // make p (which is pointing to the last node, point to the newNode)
      return;
    }

    // Case 1: if v is smaller than first node's value (has to be entered before 1st node then)
    if(p == NULL){ // if p was never incremented that means first node's value was already bigger than v
      newNode->next = c; // or h
      h = newNode; // make head point to the newNode (as it will be the new head now)
      return;
    }

    // case 2: somewhere in the middle
    // if the above cases are not true, then c is pointing to a node whose value is bigger than v
    // and p is pointing to a value smaller than v
    p->next = newNode;
    newNode->next = c;
  }
}


int main(){

  LL list;
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(7);
  list.insertAtTheEnd(10);
  list.insertAtTheEnd(15);

  printLL(list.head);

  sortedInsertion(list.head,1);
  sortedInsertion(list.head,6);
  sortedInsertion(list.head,18);

  return 0;
}