#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void deleteList();
};
LL::LL(){
  head = NULL;
}
void LL::insertAtTheEnd(int v){
  // Make a node
  Node *newNode = new Node(v);

  // if it's an empty list
  if(head == NULL){
    head = newNode; // make head point to the new node
  }
  else{
    // else traverse to the last node
    Node *c = head;

    while(c->next != NULL){
      c = c->next;
    }

    // now c points to the last node
    c->next = newNode;
  }
}

void printList(Node *head){
  // check if list is empty
  if(head == NULL){
    cout << "sorry empty list" << endl;
    return;
  }
  else{
    Node *c = head;
    while(c != NULL){
      cout << c->value << '\t';
      c = c->next;
    }
    cout << endl;
  }
}

void LL::deleteList(){
  if(head == NULL)
    return;
  else{
    Node *c = head;
    Node *n = NULL;

    while(c != NULL){
      // keep track of next node
      n = c->next;

      // delete the current node
      delete(c);

      // make next node the current node
      c = n;
    }

    // make head point to NULL
    head = NULL;
  }
}

Node *getIntersection(Node *a, Node *b){
  if(a == NULL && b != NULL)
    return b;
  else if(b == NULL && a != NULL)
    return a;
  else{
    Node dummy(0);
    Node *tail = &dummy;

    while(a != NULL && b != NULL){
      if(a->value < b->value){
        Node *newNode = new Node(a->value);

        if(tail == NULL)
          tail = newNode;
        else if(tail->value != a->value){
          tail->next = newNode;
          tail = tail->next;
        }

        a = a->next;
      }
      else if(b->value < a->value){
        Node *newNode = new Node(b->value);

        if(tail == NULL)
          tail = newNode;
        else if(tail->value != b->value){
          tail->next = newNode;
          tail = tail->next;
        }

        b = b->next;
      }
      else{
        Node *newNode = new Node(a->value);

        if(tail == NULL)
          tail = newNode;
        else if(tail->value != a->value){
          tail->next = newNode;
          tail = tail->next;
        }

        a = a->next;
        b = b->next;
      }
    }

    // add the remaining of the 2 lists as this is union
    while(a != NULL){
      Node *newNode = new Node(a->value);

      if(tail->value != a->value){
        tail->next = newNode;
        tail = tail->next;
      }

      a = a->next;
    }

    while(b != NULL){
      Node *newNode = new Node(b->value);

      if(tail->value != b->value){
        tail->next = newNode;
        tail = tail->next;
      }

      b = b->next;
    }

    return dummy.next;
  }
}

int main(){
  LL *list = new LL();

  list->insertAtTheEnd(1);
  list->insertAtTheEnd(1);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(4);
  list->insertAtTheEnd(5);
  list->insertAtTheEnd(6);

  LL *list1 = new LL();

  list1->insertAtTheEnd(1);
  list1->insertAtTheEnd(1);
  list1->insertAtTheEnd(1);
  list1->insertAtTheEnd(2);
  list1->insertAtTheEnd(4);
  list1->insertAtTheEnd(6);
  list1->insertAtTheEnd(8);
  list1->insertAtTheEnd(9);
  list1->insertAtTheEnd(10);
  list1->insertAtTheEnd(10);
  list1->insertAtTheEnd(11);
  list1->insertAtTheEnd(11);
  list1->insertAtTheEnd(12);
  list1->insertAtTheEnd(13);


  Node *intersectionList = getIntersection(list->head, list1->head);

  printList(intersectionList);

  // always delete list
  list->deleteList();

  return 0;
}
