#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void reverseInGroups(int k);
  void deleteList();
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::deleteList(){
  if(head == NULL)
    return;
  else{
    Node *c = head;
    Node *n = NULL;

    while(c != NULL){
      // keep track of next node
      n = c->next;

      // delete the current node
      delete(c);

      // make next node the current node
      c = n;
    }

    // make head point to NULL
    head = NULL;
  }
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void swap(Node *a, Node *b){
  if(a == NULL || b == NULL)
    return;
  else{
    int t = a->value;
    a->value = b->value;
    b->value = t;
  }
}

void reverse(Node* h){
  if(h == NULL || h->next == NULL)
    return;
  else{
    Node *c = h;
    Node *p = NULL;
    Node *n = NULL;

    while(c != NULL){
      n = c->next;
      c->next = p;
      p = c;
      c = n;
    }

    h = p; 
  }
}

Node* getKthFromFront(Node *n, int k){
  if(n == NULL || n->next == NULL || k == 0)
    return NULL;
  else{
    int i = 1;
    Node *c = n;

    while(c != NULL){
      if(i == k)
        break;

      i++;
      c = c->next;
    }

    if(c == NULL)
      return NULL;
    else
      return c;
  }
}

Node* getLastNode(Node *n){
  if(n == NULL)
    return NULL;
  else{
    Node *c = n;

    while(c->next != NULL)
      c = c->next;

    return c;
  }
}

// MORE UPDATED CODE - STILL NOT WORKING - FIX IT
void LL::reverseInGroups(int k){
  if(head == NULL || head->next == NULL || k == 0)
    return;
  else{
    Node *n = head;
    Node *kNode = NULL;
    Node *kNext = NULL;
    Node *nextLastNode = NULL;
    bool isHeadUpdated = false;

    while(true){
      // get the Kth node
      kNode = getKthFromFront(n,k);

      // if this is the last group
      if(kNode == NULL){
        // just reverse and leave
        // get last node
        Node *lastNode = getLastNode(n);
        // reverse the list
        reverse(n);
        // if head is not yet updated, update it (case: if this is first and only group)
        if(isHeadUpdated == false){
          head = lastNode;
          isHeadUpdated = true;
        }

        // end the loop
        break;
      }
      else{
        // kThNode is not null
        kNext = kNode->next;

        // break chain
        kNode->next = NULL;

        // keep track of the current head cos next time it'll be the last node
        // (we need to track this so that we can connect the chain again)
        nextLastNode = n;

        // reverse the list
        reverse(n);

        // join the chain again
        nextLastNode->next = kNext;

        // next iteration, kNext will be the new n
        n = kNext;
      }
    }
  }
}

int main(){
  LL l1;
  l1.insertAtTheEnd(1);
  l1.insertAtTheEnd(2);
  l1.insertAtTheEnd(3);
  l1.insertAtTheEnd(4);
  l1.insertAtTheEnd(5);
  l1.insertAtTheEnd(6);
  l1.insertAtTheEnd(7);

  l1.printLL();

  l1.reverseInGroups(3);

  l1.printLL();

  return 0;
}