bool isLoop(Node *n){
	if(n == NULL || n->next == NULL) // if 0 or 1 node return
		return false;
	else{
		Node *a = n; // point to 1st node
		Node *b = n; // point to 1st node

		while(b != NULL || b->next != NULL){
			a = a->next;
			b = b->next->next;

			if(a == b)
				return true; // if found to be the same just return true
		}

		// if we reached here that means b must have reached the end of the list
		return false;
	}
}

void removeLoop(Node *n){
	// if there are 0 or 1 nodes in the list, return
	if(n == NULL || n->next == NULL)
		return;
	else{
		// Step 1: check if there's a loop in the LL
		Node *a = n;
		Node *b = n;

		while(b != NULL && b->next != NULL){
			a = a->next;
			b = b->next->next;

			if(a == b) 
				break;
		}

		// check if there's a loop or not
		if(b == NULL || b->next == NULL)
			return;

		// Move the slower pointer back to head
		// both of them are k distance away from the loop start
		// if they move at the same pace, they should meet at the loop start
		a = n;
		while(a != b){
			a = a->next;
			b = b->next;
		}

		// both point to the start of the loop so return
		return a;
	}
}