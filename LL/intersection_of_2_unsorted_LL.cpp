#include <iostream>
#include <stdlib.h>
#include <unordered_map>
#include <vector>
using namespace std;
typedef unordered_map<int,int> Mymap;

class Node{
	public:
		int value;
		Node *next;
		Node(int v);
};
Node::Node(int v){
	value = v;
	next = NULL;
}

class LL{
	public:
		Node *head;
		LL();
		void insert(int v);
		void print();
		void reverse();
};
LL::LL(){
	head = NULL;
}
void LL::insert(int v){
	// make a new node
	Node *newNode = new Node(v);

	// Case 1: 0 node in list
	if(head == NULL)
		head = newNode;
	// Case 2: 1 node in the list
	else if(head->next == NULL)
		head->next = newNode;
	// Case 3: traverse till the end
	else{
		Node *c = head;
		while(c->next != NULL)
			c = c->next;
		c->next = newNode;
	}
}
void LL::print(){
	if(head == NULL)
		return;
	else{
		Node *c = head;
		while(c != NULL){
			cout << c->value << '\t';
			c = c->next;
		}
		cout << endl;
	}
}
void printVector(vector<int> v){
	for(int i = 0; i < v.size(); i++)
		cout << v[i] << '\t';
	cout << endl;
}

void printIntersection(Node *h1, Node *h2){
	if(h1 == NULL && h2 == NULL)
		return;
	else{
		vector<int> intersection;

		// Put the first list elements in the hash + in the union vector
		Mymap m;
		Mymap::iterator it;

		Node *c = h1;
		while(c != NULL){
			m.insert(Mymap::value_type(c->value,1)); // just in hash, not in the result list
			c = c->next;
		}

		// Traverse the 2nd list + put the elements that are in the hash (As they are the common ones)
		c = h2;

		while(c != NULL){
			it = m.find(c->value);

			if(it != m.end()) // in the hash
				intersection.push_back(c->value);

			c = c->next;
		}

		printVector(intersection);
	}
}
int main(){
	LL *list1 = new LL();
	LL *list2 = new LL();

	list1->insert(10);
	list1->insert(15);
	list1->insert(4);
	list1->insert(20);

	list2->insert(8);
	list2->insert(4);
	list2->insert(2);
	list2->insert(10);

	printIntersection(list1->head, list2->head);
	
	return 0;
}