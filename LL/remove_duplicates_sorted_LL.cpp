void removeDuplicates(Node *n){
  if(n == NULL || n->next == NULL) // return if there's only 1 or 0 node in the list
    return;

  else{
    // can't assign the first ptr (a) to NULL cos then we wont be able to compare its value to b's for the first element
    Node *a = n; 
    Node *b = n->next;

    // traverse the list
    while(b != NULL){
      // if value are same
      if(a->value == b->value){
        Node *t = b;
        a->next = b->next;
        b = b->next; 
        delete t;
      }
      else{ // if not same, just iterate
        a = b;
        b = b->next;
      }
    }
  }
}