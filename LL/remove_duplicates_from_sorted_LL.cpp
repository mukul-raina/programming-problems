void deleteDuplicates(Node *n){
  if(n == NULL || n->next == NULL) // if 0 or 1 node in list, just return
    return;
  else{
    // Declare the hash
    Mymap m;
    Mymap::iterator iter;

    // 2 running pointers
    Node *c = n;
    Node *p = NULL;

    // Traverse the list
    while(c != NULL){
      iter = m.find(c->value); // check if the element is in the hash

      // if not in the hash, insert it + increment both the running pointers
      if(iter == m.end()){
        m.insert(Mymap::value_type(c->value,1));
        p = c;
        c = c->next;
      }

      // if found in the hash, that means duplicate so remove it
      else{
        Node *t = c;
        p->next = c->next;
        c = c->next;
        delete t;
      }
    }
  }
}