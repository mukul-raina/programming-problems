#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void deleteList();
};
LL::LL(){
  head = NULL;
}
void LL::insertAtTheEnd(int v){
  // Make a node
  Node *newNode = new Node(v);

  // if it's an empty list
  if(head == NULL){
    head = newNode; // make head point to the new node
  }
  else{
    // else traverse to the last node
    Node *c = head;

    while(c->next != NULL){
      c = c->next;
    }

    // now c points to the last node
    c->next = newNode;
  }
}

void printList(Node *head){
  // check if list is empty
  if(head == NULL){
    cout << "sorry empty list" << endl;
    return;
  }
  else{
    Node *c = head;
    while(c != NULL){
      cout << c->value << '\t';
      c = c->next;
    }
    cout << endl;
  }
}

void LL::deleteList(){
  if(head == NULL)
    return;
  else{
    Node *c = head;
    Node *n = NULL;

    while(c != NULL){
      // keep track of next node
      n = c->next;

      // delete the current node
      delete(c);

      // make next node the current node
      c = n;
    }

    // make head point to NULL
    head = NULL;
  }
}

Node *getIntersection(Node *a, Node *b){
  if(a == NULL || b == NULL)
    return NULL;

  // make a dummy node 
  // give a random value to it, it doesnt matter
  // cos the result will be dummy->next that means
  // the rest of the list excluding the first node (Which is dummy)
  Node dummy(0);
  Node *tail = &dummy;

  while(a != NULL && b != NULL){
    if(a->value == b->value){
      // Make a new node 
        Node *newNode = new Node(a->value);

      // if the list is empty, then push the common element and leave
      if(tail == NULL)
        tail = newNode;
      
      else if(a->value != tail->value){
        // make the tail node point to the newNode
        // there's a difference between tail node + dummy node
        // dummy node is the first node of the result list, tail points to the last node of the list 
        // which we keep incrementing
        tail->next = newNode;

        // incremennt 
        tail = tail->next;
      }

      a = a->next;
      b = b->next;
    }
    else if(a->value < b->value)
      a = a->next;
    else
      b = b->next;
  }

  return dummy.next;
}

int main(){
  LL *list = new LL();

  list->insertAtTheEnd(1);
  list->insertAtTheEnd(1);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(4);
  list->insertAtTheEnd(5);
  list->insertAtTheEnd(6);

  LL *list1 = new LL();

  list1->insertAtTheEnd(1);
  list1->insertAtTheEnd(1);
  list1->insertAtTheEnd(1);
  list1->insertAtTheEnd(2);
  list1->insertAtTheEnd(4);
  list1->insertAtTheEnd(6);
  list1->insertAtTheEnd(8);

  Node *intersectionList = getIntersection(list->head, list1->head);

  printList(intersectionList);

  // always delete list
  list->deleteList();

  return 0;
}
