#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void deleteList();
  void partition(Node *n, int v);
};
LL::LL(){
  head = NULL;
}
void LL::insertAtTheEnd(int v){
  // Make a node
  Node *newNode = new Node(v);

  // if it's an empty list
  if(head == NULL){
    head = newNode; // make head point to the new node
  }
  else{
    // else traverse to the last node
    Node *c = head;

    while(c->next != NULL){
      c = c->next;
    }

    // now c points to the last node
    c->next = newNode;
  }
}

void printList(Node *head){
  // check if list is empty
  if(head == NULL){
    cout << "sorry empty list" << endl;
    return;
  }
  else{
    Node *c = head;
    while(c != NULL){
      cout << c->value << '\t';
      c = c->next;
    }
    cout << endl;
  }
}

void LL::deleteList(){
  if(head == NULL)
    return;
  else{
    Node *c = head;
    Node *n = NULL;

    while(c != NULL){
      // keep track of next node
      n = c->next;

      // delete the current node
      delete(c);

      // make next node the current node
      c = n;
    }

    // make head point to NULL
    head = NULL;
  }
}

// merge b into a
void merge(Node * &a, Node * &b){
  if(a == NULL || b == NULL)
    return;

  Node *first = a;
  Node *second = b;
  Node *firstNext = NULL;
  Node *secondNext = NULL;

  while(first != NULL){
    firstNext = first->next;
    secondNext = second->next;

    first->next = second;
    second->next = firstNext;

    first = firstNext;
    second = secondNext;
  } 

  b = secondNext;
}


int main(){
  LL *list = new LL();

  list->insertAtTheEnd(1);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(3);
  
  LL *list1 = new LL();

  list1->insertAtTheEnd(4);
  list1->insertAtTheEnd(5);
  list1->insertAtTheEnd(6);
  list1->insertAtTheEnd(7);
  list1->insertAtTheEnd(8);


  printList(list->head);
  printList(list1->head);

  merge(list->head, list1->head);

  printList(list->head);
  printList(list1->head);
  
  // always delete list
  list->deleteList();

  return 0;
}
