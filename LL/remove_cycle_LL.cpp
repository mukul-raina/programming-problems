void LL::removeCycle(){
	if(head == NULL || head->next == NULL)
		return;
	else{
		// check if there's a cycle
		Node *a = head;
		Node *b = head;

		while(b != NULL && b->next != NULL){
			a = a->next;
			b = b->next->next;

			if(a == b)
				break;
		}

		if(a != b)
			return; // no cycle was there in the first place
		else{
			// make a point to the head
			a = head;
			while(a != b){
				a = a->next;
				b = b->next;
			}

			// both now point to the start of the loop
			return b; // or a
		}
	}
}