#include <iostream>
using namespace std;

class Node{
public:
	int value;
	Node *next;
	Node(int v);
};
Node::Node(int v){
	value = v;
	next = NULL;
}

class LL{
public:
	Node *head;
	LL();
	void insertAtTheEnd(int v);
	void insertAtTheStart(int v);
	void printList();
	void deleteList();
};
LL::LL(){
	head = NULL;
}
void LL::insertAtTheEnd(int v){
	// Make a node
	Node *newNode = new Node(v);

	// if it's an empty list
	if(head == NULL){
		head = newNode; // make head point to the new node
	}
	else{
		// else traverse to the last node
		Node *c = head;

		while(c->next != NULL){
			c = c->next;
		}

		// now c points to the last node
		c->next = newNode;
	}
}

void LL::insertAtTheStart(int v){
	// make the node
	Node *newNode = new Node(v);

	if(head == NULL)
		head = newNode;
	else{
		// make the newNode point to the first node
		newNode->next = head;

		// make head point to the newNode
		head = newNode;
	}
}


void LL::printList(){
	// check if list is empty
	if(head == NULL){
		cout << "sorry empty list" << endl;
		return;
	}
	else{
		Node *c = head;
		while(c != NULL){
			cout << c->value << '\t';
			c = c->next;
		}
		cout << endl;
	}
}

void LL::deleteList(){
	if(head == NULL)
		return;
	else{
		Node *c = head;
		Node *n = NULL;

		while(c != NULL){
			// keep track of next node
			n = c->next;

			// delete the current node
			delete(c);

			// make next node the current node
			c = n;
		}

		// make head point to NULL
		head = NULL;
	}
}

int main(){
	LL *list = new LL();

	list->insertAtTheStart(1);
	list->insertAtTheStart(2);
	list->insertAtTheStart(3);
	list->insertAtTheStart(4);

	list->printList();

	// always delete list
	list->deleteList();

	return 0;
}

