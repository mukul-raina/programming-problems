#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printList();
  void deleteList();
  void rotate(Node *n, int k);
};
LL::LL(){
  head = NULL;
}
void LL::insertAtTheEnd(int v){
  // Make a node
  Node *newNode = new Node(v);

  // if it's an empty list
  if(head == NULL){
    head = newNode; // make head point to the new node
  }
  else{
    // else traverse to the last node
    Node *c = head;

    while(c->next != NULL){
      c = c->next;
    }

    // now c points to the last node
    c->next = newNode;
  }
}

void LL::printList(){
  // check if list is empty
  if(head == NULL){
    cout << "sorry empty list" << endl;
    return;
  }
  else{
    Node *c = head;
    while(c != NULL){
      cout << c->value << '\t';
      c = c->next;
    }
    cout << endl;
  }
}

void LL::deleteList(){
  if(head == NULL)
    return;
  else{
    Node *c = head;
    Node *n = NULL;

    while(c != NULL){
      // keep track of next node
      n = c->next;

      // delete the current node
      delete(c);

      // make next node the current node
      c = n;
    }

    // make head point to NULL
    head = NULL;
  }
}

Node * getKthNode(Node *n, int k){
  if(n == NULL)
    return NULL;
  else{
    int i = 1;
    Node *c = n;

    while(c != NULL){
      if(i == k)
        break;

      i++;
      c = c->next;
    }

    // return c in both cases
    if(c == NULL)
      return c; // c reached the end, k was too long 
    else 
      return c; // c pointint to the kth node
  }
}

Node * getLastNode(Node *n){
  if(n == NULL)
    return NULL;
  else{
    Node *c = n;

    while(c->next != NULL)
      c = c->next;

    return c; // c points to the last node
  }
}

void LL::rotate(Node *n, int k){
  // Case 1: dont do anything if empty list, single node list or k is 0
  if(n == NULL || n->next == NULL || k == 0)
    return;
  else{
    // get the kTh node (from front)
    Node *kthNode = getKthNode(n,k);

    // if k was too big or k was the last node, don't do anything
    if(kthNode == NULL || kthNode->next == NULL)
      return;
    else{
      // get the last node now
      Node *l = getLastNode(n);

      // make the last node point to the head
      l->next = head;

      // make head point to the node after kth
      head = kthNode->next;

      // break the chain
      kthNode->next = NULL;
    }
  }
}

int main(){
  LL *list = new LL();

  list->insertAtTheEnd(1);
  list->insertAtTheEnd(2);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(4);
  list->insertAtTheEnd(5);
  list->insertAtTheEnd(6);

  list->printList();

  list->rotate(list->head,4);

  list->printList();

  // always delete list
  list->deleteList();

  return 0;
}
