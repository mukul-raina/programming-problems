bool isLoop(Node *n){
	if(n == NULL || n->next == NULL) // if 0 or 1 node return
		return false;
	else{
		Node *a = n; // point to 1st node
		Node *b = n; // point to 1st node

		while(b != NULL || b->next != NULL){
			a = a->next;
			b = b->next->next;

			if(a == b)
				return true; // if found to be the same just return true
		}

		// if we reached here that means b must have reached the end of the list
		return false;
	}
}