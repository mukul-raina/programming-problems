#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void reverse();
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void printLL(Node *n){
  if(n == NULL)
    return;

  Node *c = n;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void LL::reverse(){
  // if 0 or 1 node, return
  if(head == NULL || head->next == NULL)
    return;
  else{
    Node *c = head;
    Node *p = NULL;
    Node *n = NULL;

    while(c != NULL){
      n = c->next;
      c->next = p;
      p = c;
      c = n;
    }

    head = p;
  }
}

int main(){

  LL list;
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(7);
  list.insertAtTheEnd(10);
  list.insertAtTheEnd(15);

  cout << "list before reversing" << endl;
  printLL(list.head);

  list.reverse();

  cout << "list after reversing: " << endl;
  printLL(list.head);

  return 0;
}