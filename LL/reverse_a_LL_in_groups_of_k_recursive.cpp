#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

// Reverse the LL in groups of K and return a pointer to the new head node
Node* reverseInGroups(Node* h, int k){
  Node *c = h;
  Node *n = NULL;
  Node *p = NULL;
  int i = 0;

  // Reverse the first k nodes of the LL
  while(c != NULL && i < k){
    n = c->next;
    c->next = p;
    p = c;
    c = n;
    i++;
  }
  // n now points to the k+1 node
  // h still points to 1 and we want 1->next to be 
  // reverseInGroups(4,5,6,7) and that will return us
  // a pointer to 6 5 4 7 so the resultant list would be 
  // 3 2 1 6 5 4 7

  // Note: 4 does NOT point to 3 after end of the first iteration
  // after end of the while loop, c and n merely point to 4 in hopes that in the next iteration of while loop 
  // they would point c to p, but that never happened as loop ended by then due to i being equal to k
  if(n != NULL)
    h->next = reverseInGroups(n,k); // or c (both point to the same node at this point i.e. 4 n 7)

  // prev is the new head of the input list
  return p;
}

int main(){
  LL l1;
  l1.insertAtTheEnd(1);
  l1.insertAtTheEnd(2);
  l1.insertAtTheEnd(3);
  l1.insertAtTheEnd(4);
  l1.insertAtTheEnd(5);
  l1.insertAtTheEnd(6);
  l1.insertAtTheEnd(7);
  l1.insertAtTheEnd(8);

  l1.printLL();

  l1.head = reverseInGroups(l1.head,3);

  l1.printLL();

  return 0;
}