#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void deleteList();
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void printLL(Node *n){
  if(n == NULL)
    return;

  Node *c = n;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

int getKthFromFront(Node *n, int k){
  if(n == NULL)
    return -1;
  else{
    int i = 1;
    Node *c = n;

    while(c != NULL){
      if(i == k)
        break;

      i++;
      c = c->next;
    }

    if(c == NULL)
      return -1;
    else
      return c->value;
  }
}

void LL::deleteList(){
  if(head == NULL)
    return;
  else{
    Node *c = head;
    Node *n = NULL;

    while(c != NULL){
      // keep track of next node
      n = c->next;

      // delete the current node
      delete(c);

      // make next node the current node
      c = n;
    }

    // make head point to NULL
    head = NULL;
  }
}

int main(){

  LL list;
  list.insertAtTheEnd(2);
  list.insertAtTheEnd(5);
  list.insertAtTheEnd(7);
  list.insertAtTheEnd(10);
  list.insertAtTheEnd(15);

  printLL(list.head);

  cout << getKthFromFront(list.head,3) << endl;
  cout << getKthFromFront(list.head,1) << endl;
  cout << getKthFromFront(list.head,8) << endl;
  cout << getKthFromFront(list.head,0) << endl;

  // always delete list
  //list->deleteList();

  return 0;
}