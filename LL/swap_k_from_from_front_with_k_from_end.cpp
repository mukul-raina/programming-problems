#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
  void deleteList();
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void LL::deleteList(){
  if(head == NULL)
    return;
  else{
    Node *c = head;
    Node *n = NULL;

    while(c != NULL){
      // keep track of next node
      n = c->next;

      // delete the current node
      delete(c);

      // make next node the current node
      c = n;
    }

    // make head point to NULL
    head = NULL;
  }
}

void LL::printLL(){
  if(head == NULL)
    return;

  Node *c = head;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

void swapKth(Node *&n, int k, int size){
  // check if k is valid
  if(size < k)
    return;

  // if x and y are the same, return
  if((2 * k - 1) == size)
    return;

  // find the kth node from front + its prev
  Node *x = n;
  Node *x_prev = NULL;

  for(int i = 1; i < k; i++){
    x_prev = x;
    x = x->next;
  }

  // find the kth node from the end + its prev
  Node *y = n;
  Node *y_prev = NULL;

  for(int i = 1; i < n-k+1; i++){
    y_prev = y;
    y = y->next;
  }

  if(x_prev)
    x_prev->next = y;

  if(y_prev)
    y_prev->next = x;

  Node *t = x->next;
  x->next = y->next;
  y->next = t;

  // change head pointers if k is 1 or n
  if(k == 1)
    n = y;
  if(k == n)
    n = x;
}


int main(){
  LL l1;
  l1.insertAtTheEnd(1);
  l1.insertAtTheEnd(2);
  l1.insertAtTheEnd(3);
  l1.insertAtTheEnd(4);
  l1.insertAtTheEnd(5);
  l1.insertAtTheEnd(6);
  l1.insertAtTheEnd(7);
  l1.insertAtTheEnd(8);

  l1.printLL();

  swapKth(l1.head,3,8);

  l1.printLL();

  return 0;
}