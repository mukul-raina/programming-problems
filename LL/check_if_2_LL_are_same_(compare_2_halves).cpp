#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void printLL();
};
LL::LL(){
  head = NULL;
}

// Time: O(n)
void LL::insertAtTheEnd(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: 0 node in the list
  if(head == NULL)
    head = newNode;
  
  // Case 2: 1 node in the list
  else if(head->next == NULL)
    head->next = newNode;

  // Case 3: More than 1 node in the list
  else{
    // Traverse to the end of the list
    Node *c = head;
    while(c->next != NULL){
      c = c->next;
    }

    // Now c points to the last node of the list
    // Make the last node point to the newNode
    c->next = newNode;
  } 
}

void printLL(Node *n){
  if(n == NULL)
    return;

  Node *c = n;
  while(c != NULL){
    cout << c->value << '\t';
    c = c->next;
  }
  cout << endl;
}

bool compareTwoHalves(Node *a, Node *b){
  if(a == NULL && b == NULL) // both are empty
    return true;
  else if(a == NULL) // only 1 is empty
    return false;
  else if(b == NULL) // only 1 is empty
    return false;
    // none of them are empty
  else{
    Node *p = a;
    Node *q = b;

    // Traverse both the LL (until one of them ends)
    while(p != NULL && q != NULL){
      // if at any point the value of corresponding nodes are not same, return false
      if(p->value != q->value) 
        return false;

      p = p->next;
      q = q->next;
    }

    // if both of them ended at the same time, return true
    // else return false (as one list is longer than the other)
    if(p == NULL && q == NULL)
      return true;
    else
      return false;
  }
}

int main(){

  LL list1;
  list1.insertAtTheEnd(1);
  list1.insertAtTheEnd(2);
  list1.insertAtTheEnd(3);
  list1.insertAtTheEnd(4);
  list1.insertAtTheEnd(3);

  LL list2;
  list2.insertAtTheEnd(1);
  list2.insertAtTheEnd(2);
  list2.insertAtTheEnd(3);
  list2.insertAtTheEnd(4);
  list2.insertAtTheEnd(5);

  cout << compareTwoHalves(list1.head,list2.head) << endl;

  return 0;
}