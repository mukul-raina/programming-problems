#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *next;
  Node(int v);
};
Node::Node(int v){
  value = v;
  next = NULL;
}

class LL{
public:
  Node *head;
  LL();
  void insertAtTheEnd(int v);
  void deleteList();
  void partition(Node *n, int v);
};
LL::LL(){
  head = NULL;
}
void LL::insertAtTheEnd(int v){
  // Make a node
  Node *newNode = new Node(v);

  // if it's an empty list
  if(head == NULL){
    head = newNode; // make head point to the new node
  }
  else{
    // else traverse to the last node
    Node *c = head;

    while(c->next != NULL){
      c = c->next;
    }

    // now c points to the last node
    c->next = newNode;
  }
}

void printList(Node *head){
  // check if list is empty
  if(head == NULL){
    cout << "sorry empty list" << endl;
    return;
  }
  else{
    Node *c = head;
    while(c != NULL){
      cout << c->value << '\t';
      c = c->next;
    }
    cout << endl;
  }
}

void LL::deleteList(){
  if(head == NULL)
    return;
  else{
    Node *c = head;
    Node *n = NULL;

    while(c != NULL){
      // keep track of next node
      n = c->next;

      // delete the current node
      delete(c);

      // make next node the current node
      c = n;
    }

    // make head point to NULL
    head = NULL;
  }
}

void LL::partition(Node *n, int x){
  if(n == NULL || n->next == NULL)
    return;

  queue<int> lowerQ;
  queue<int> upperQ;

  Node *c = n;

  while(c != NULL){
    if(c->value < x)
      lowerQ.push(c->value);
    if(c->value > x)
      upperQ.push(c->value);

    c = c->next;
  }

  // overwrite
  c = head;

  while(lowerQ.empty() != true){
    c->value = lowerQ.front();
    lowerQ.pop();
    c = c->next;
  } 

  c->value = x;
  c = c->next;

  while(upperQ.empty() != true){
  c->value = upperQ.front();
  upperQ.pop();
  c = c->next;
  } 
}


int main(){
  LL *list = new LL();

  list->insertAtTheEnd(2);
  list->insertAtTheEnd(1);
  list->insertAtTheEnd(4);
  list->insertAtTheEnd(6);
  list->insertAtTheEnd(3);
  list->insertAtTheEnd(8);
  list->insertAtTheEnd(5);
  list->insertAtTheEnd(7);

  printList(list->head);

  list->partition(list->head, 4);

  printList(list->head);

  // always delete list
  list->deleteList();

  return 0;
}
