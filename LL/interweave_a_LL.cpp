// Interweave a linked list. 
//Do it in Linear time and constant space. 
//Input: A->B->C->D->E Output: A->E->B->D->C

void interweave(Node* &n){
	// Step 1: Get the middle of the LL
	Node *m = getMiddleNode(n); // will return a pointer to C

	// Step 2: Reverse the second half of the LL
	reverse(m->next); // Will turn the LL into A->B->C->E->D

	// Step 3: Join 2 LL at alternate positions
	// break the list after mid first
	Node *secondHalfHead = mid->next; // E->D
	Node *firstHalfHead = n; // A->B->C
	mid->next = NULL;

	join2LLAtAlternatePositions(firstHalfHead,secondHalfHead); // A->E->B->D->C
}
