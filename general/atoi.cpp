#include <iostream>
using namespace std;

/*
	// we do (v - '0') cos the difference between the ascii value of 3 and 
	// ascii value of 0 will be 3 BUT now the difference is in number 
	// so in a way we converted 3 which was char to 3 which is a number now
*/
int myAtoi(string s){
	int n = 0;
	int sign = 1;
	int startingIndex = 0;

	if(s[0] == '-'){
		sign = -1;
		startingIndex = 1;
	}

	for(int i = startingIndex; i < s.size(); i++){
		int v = s[i]; // to get the ascii value of 1 (which is char now)

		n = (n * 10) + (v - '0');
	}

	return sign * n;
}

int main(){
	string s = "12345";
	string s1 = "-12345";

	cout << myAtoi(s) << endl;
	cout << myAtoi(s1) << endl;

	return 0;
}