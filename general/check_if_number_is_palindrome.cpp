#include <iostream>
using namespace std;

int getReverseNum(int n){

	int revNum = 0;

	while(n > 0){
		int digit = n % 10;
		revNum = (revNum * 10) + digit;
		n = n / 10;
	}

	return revNum;
}

bool isPalindrome(int n){
	int reversedNum = getReverseNum(n);

	if(n == reversedNum)
		return true;
	else
		return false;
}

int main(){
	int n = 12321;

	cout << isPalindrome(n) << endl;

	return 0;
}