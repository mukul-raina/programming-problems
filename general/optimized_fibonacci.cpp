#include <iostream>
using namespace std;

void initializeAllValuesToZero(int *a, int size){
	if(size == 0)
		return;
	else{
		for(int i = 0; i < size; i++)
			a[i] = 0;
	}
}

// atleast we know there are first 2 elements
void getFibHelper(int *fibArray, int size){
	fibArray[0] = 0;
	fibArray[1] = 1;

	for(int i = 2; i < size; i++)
		fibArray[i] = fibArray[i - 1] + fibArray[i - 2];
}

int getFib(int num){
	if(num == 0 || num == 1)
		return num;
	else{
		int size = num + 1; // if we want 6th fib number, thats actually the 7th element
		int fibArray[size]; // make 7 elemetns (6 + 1)

		initializeAllValuesToZero(fibArray,size);

		getFibHelper(fibArray,size);

		return fibArray[size - 1];
	}
}

int main(){
	int num = 6;

	cout << getFib(num) << endl;

	return 0;
}