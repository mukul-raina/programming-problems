#include <iostream>
using namespace std;

bool isMultipleOf3(int n){
	return (n % 3 == 0);
}

bool isMultipleOf5(int n){
	return (n % 5 == 0);
}

void fizzbuzz(){
	for(int i = 1; i <= 100; i++){
		
		if(isMultipleOf3(i) && isMultipleOf5(i))
			cout << i << ": fizzbuzz" << endl;
		
		else if(isMultipleOf3(i))
			cout << i << " : Fizz" << endl;
		
		else if(isMultipleOf5(i))
			cout << i << " : Buzz" << endl;
		
		else
			cout << i << endl;
	}
}

int main(){
	fizzbuzz();

	return 0;
}