#include <iostream>
using namespace std;

bool isMultipleOf3(int n){
	return (n % 3 == 0);
}

bool isMultipleOf5(int n){
	return (n % 5 == 0);
}

void fizzbuzz(int n){
	if(n > 100)
		return;
	else{
		if(isMultipleOf3(n) && isMultipleOf5(n)){
			cout << n << ": fizzbuzz" << endl;
			fizzbuzz(n + 1);
		}
		
		else if(isMultipleOf3(n)){
			cout << n << " : Fizz" << endl;
			fizzbuzz(n + 1);
		}
		
		else if(isMultipleOf5(n)){
			cout << n << " : Buzz" << endl;
			fizzbuzz(n + 1);
		}
		
		else{
			cout << n << endl;
			fizzbuzz(n + 1);
		}
	}
}

int main(){
	int n = 1;

	fizzbuzz(n);

	return 0;
}