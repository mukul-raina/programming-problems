#include <iostream>
using namespace std;

int decimalToBinary(int n){
	int rem;
	int i = 1;
	int binary = 0;

	while(n != 0){
		rem = n % 2;
		n = n / 2;
		binary = binary + (rem * i);
		i = i * 10;
	}

	return binary;
}

int main(){
	int value = 80;

	cout << decimalToBinary(value) << endl;
	
	return 0;
}