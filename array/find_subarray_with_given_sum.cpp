int subArraySum(int *a, int n, int sum){
	int curr_sum = a[0];
	int start = 0;
	int i;

	for(i = 1; i <= n; i++){
		while(curr_sum > sum && start < i - 1){
			curr_sum = curr_sum - a[start];
			start++;
		}

		if(curr_sum == sum)
			cout << "between " << start << " and " << i - 1 << endl;

		if(i < n)
			curr_sum = curr_sum + a[i];
	}

	// If we reach here then no such subarray was found
	return -1;	
}