#include <iostream>
using namespace std;

// Time: O(log n)
int getFixedPoint(int *a, int l, int h){
	if(l > h)
		return -1;
	else{
		int m = (l + h) / 2;

		// value is equal to index
		if(a[m] == m)
			return m;

		// value is less than the index - go right
		else if(a[m] < m)
			return getFixedPoint(a,m+1,h);

		// value is greated than the index - go left
		else
			return getFixedPoint(a,l,m-1);
	}
}

void testCase1(){
	int a[] = {-10,-5,0,3,7};
	int sizeA = 5;

	cout << getFixedPoint(a,0,sizeA) << endl;
}

void testCase2(){
	int a[] = {-10, -1, 0, 3, 10, 11, 30, 50, 100};
	int sizeA = 10;

	cout << getFixedPoint(a,0,sizeA) << endl;
}

void testCase3(){
	int a[] = {0, 2, 5, 8, 17};
	int sizeA = 5;

	cout << getFixedPoint(a,0,sizeA) << endl;
}

void testCase4(){
	int a[] = {-10, -5, 3, 4, 7, 9};
	int sizeA = 6;

	cout << getFixedPoint(a,0,sizeA) << endl;
}


int main(){
	testCase1();
	testCase2();
	testCase3();
	testCase4();
	return 0;
}