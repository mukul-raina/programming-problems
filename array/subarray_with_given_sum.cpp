#include <iostream>
using namespace std;

void subarrayWithGivenSum(int *a, int size, int givenSum){
	if(size == 0)
		return;

	int currSum = a[0]; // current sum is the first element
	int j = 0; // starting point is the first element
	int i;
	int n = size -1;
	
	for(int i = 1; i <= n; i++){

		// if the currSum exceeds the givenSum
		while(currSum > givenSum && j < i - 1){
			currSum = currSum - a[j];
			j++;
		}

		// if currSum becomes equal to sum
		if(currSum == givenSum){
			cout << "starting index: " << j << endl;
			cout << "ending index: " << i - 1 << endl;
			return;
		}

		// Add this element to the currSum
		if(i < n)
			currSum = currSum + a[i];
	}

	// if we reached over here, nothing was found
	cout << "sorry no subarray was found" << endl;
}

int main(){
	int a[] = {15, 2, 4, 8, 9, 5, 10, 23};
	int size = 8;
	int sum = 23;

	subarrayWithGivenSum(a, size, sum);

	return 0;
}