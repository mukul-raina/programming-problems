int rotatedBinarySearch(int *a, int size, int x){
	int low = 0;
	int high = size - 1;

	while(low <= high){
		// get mid
		int mid = (low + high) / 2;

		// check if mid is the x
		if(a[mid] == x)
			return mid;

		// if bottom half is sorted
		if(a[low] <= a[mid]){
			if(a[low] <= x && x < a[mid])
				high = mid - 1;
			else
				low = mid + 1;
		}

		// if upper half is sorted
		else{
			if(a[mid] < x && x <=a[high])
				low = mid + 1;
			else
				high = mid - 1;
		}
	}
	return -1;
}