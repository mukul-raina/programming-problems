#include <iostream>
using namespace std;

void printArray(int *a, int size){
	if(size == 0)
		return;
	else{
		for(int i = 0; i < size; i++)
			cout << a[i] << '\t';
		cout << endl;
	}
}

void reverseArray(int *a, int size){
	int i = 0; 
	int j = size - 1; 

	while(i < j){
		swap(a[i],a[j]);
		i++;
		j--;
	}
}

int main(){
	int a[] = {1,2,3,4,5};
	
	printArray(a,5);
	reverseArray(a,5);
	printArray(a,5);
	
	return 0;
}