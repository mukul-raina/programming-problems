#include <iostream>
using namespace std;

void swap(int &a, int &b){
    int t = a;
    a = b;
    b = t;
}

void sort(int *a, int s){
  if(s == 0 || s == 1)
    return;
  else{
    int i = 0;
    int j = s - 1;

    while(i < j){
      // make i point to the first 1
      while(a[i] == 0 && i < j)
        i++;

      // make j point to the first 0
      while(a[j] == 1 && i < j)
        j--;

      //now swap the value
      if(i < j){
        swap(a[i],a[j]);
        i++;
        j--;
      }
    }
  }
}


void printArray(int *a, int s){
  if(s == 0)
    return;
  else{
    for(int i = 0; i < s; i++){
      cout << a[i] << '\t';
    }
    cout << endl;
  }
}

int main(){
  int a[] = {1,0,1,1,0,0,0,1,0,0,0,1};
  int size = 12;

  sort(a,size);
  printArray(a,size);

  return 0;
}