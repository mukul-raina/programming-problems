#include <iostream>
#include <vector>
using namespace std;

void printVector(vector<int> a){
  if(a.size() == 0)
    return;
  else{
    for(int i = 0; i < a.size(); i++)
      cout << a[i] << '\t';
  }
  cout << endl;
}

void printUnion(int *a, int aSize, int *b, int bSize){
  if(aSize == 0 && bSize == 0)
    return;
  else{
    
    vector<int> unionVector;

    int i = 0;
    int j = 0;

    while(i < aSize && j < bSize){
      if(a[i] < b[j]){
        unionVector.push_back(a[i]);
        i++;
      }
      else if(b[j] < a[i]){
        unionVector.push_back(b[j]);
        j++;
      }
      else{
        // both of the numbers are equal
        unionVector.push_back(a[i]);
        i++;
        j++;
      }
    }

    // lets say if 1 array wasn't complete
    while(i < aSize){
      unionVector.push_back(a[i]);
      i++;
    }

    while(j < bSize){
      unionVector.push_back(b[j]);
      j++;
    }

    printVector(unionVector);
  }
}

int main(){
  int a[] = {1,3,4,5,7};
  int b[] = {2,3,5,6};

  printUnion(a,5,b,4);

  return 0;
}