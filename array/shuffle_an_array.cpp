#include <iostream>
#include <stdlib.h>
using namespace std;

void shuffle(int *a, int size){
	if(size == 0 || size == 1)
		return;

	srand(time(NULL));

	// get a random index
	// swap it with the current index
	// keep doing it until you have traversed the array
	for(int i = size - 1; i > 0; i--){
		// pick random index from 0 to i
		int j = rand() % (i + 1);

		// swap a[i] with the element at the random index
		swap(a[i],a[j]);
	}
}

void printArray(int *a, int size){
	for(int i = 0; i < size; i++)
		cout << a[i] << '\t';
	cout << endl;
}

int main(){
	int a[] = {1, 2, 3, 4, 5, 6, 7, 8};
	int size = 8;

	shuffle(a,size);

	printArray(a,size);

	return 0;
}