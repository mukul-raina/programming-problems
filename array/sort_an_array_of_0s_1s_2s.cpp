void sort(int *a, int size){
	int i = 0;
	int j = size - 1;
	int k = 0;

	// traverse k
	// i keeps track of the location where we put 0s (on left)
	// j keeps track of the location where we put 2s (on right)
	// 1s goes in the middle
	while (k <= j){
		switch(a[k]){
			case 0:
			{
				swap(a[i],a[k]);
				i++;
				k++;
				break;
			}

			case 1:
			{
				k++;
				break;
			}

			case 2:
			{
				swap(a[k],a[j]);
				j--;
				break;
			}
		}
	}
}