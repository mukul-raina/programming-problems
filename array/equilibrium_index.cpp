int getEquilibriumIndex(int a*, int size)
{
   int sum = 0;      // initialize sum of whole array
   int l = 0; // initialize leftsum
   int i;
 
   // Find sum of the whole array
   for (i = 0; i < n; ++i)
        sum += a[i];
 
  // Traverse the array again
   for( i = 0; i < n; ++i)
   {
      sum -= a[i]; // sum is now right sum for index i
 
      if(l == sum)
        return i;
 
      l += a[i];
   }
 
    // If no equilibrium index was found
    return -1;
}