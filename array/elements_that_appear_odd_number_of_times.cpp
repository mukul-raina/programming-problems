void elementsThatOccurOddNumberOfTimes(int *a, int size){
    if(size == 0)
        return;

    else if(size == 1)
        cout << a[0] << endl;

    else{
        Mymap m;
        Mymap::iterator it;

        for(int i = 0; i < size; i++){
            it = m.find(a[i]); // find the element
            if(it == m.end())
                m.insert(Mymap::value_type(a[i],1)); // if not found, insert + counter is 1 now
            else
                it->second++; // if found increase the counter
        }

        for(int i = 0; i < size; i++){
            it = m.find(a[i]); // lookup the element in the hash
            if(it->second % 2 != 0) // check if its # of occurence is odd
                cout << it->first << endl;
        }
    }
}