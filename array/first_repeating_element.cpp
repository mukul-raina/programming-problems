#include<iostream>
#include<unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

int getFirstRepeatingElement(int *a, int size){
	if(size == 0)
		return -1;
	else{
		Mymap hash;
		Mymap::iterator it;
		int minIndex = INT_MAX;

		for(int i = 0; i < size; i++){
			it = hash.find(a[i]);

			if(it == hash.end())
				hash.insert(Mymap::value_type(a[i],i));
			else{
				if(it->second < minIndex)
					minIndex = it->second;
			}
		}

		return a[minIndex];
	}
}

int main(){
	int a[] = {6,10,5,4,9,1,4,6,10};
	int size = 9;

	cout << getFirstRepeatingElement(a,size) << endl;
	return 0;
}
