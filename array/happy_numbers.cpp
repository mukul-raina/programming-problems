#include <iostream>
#include <unordered_map>
using namespace std;
typedef unordered_map<int,int> Mymap;

// Time: O(n)
int sumOfSquareOfDigits(int n){
	int sum = 0;

	// keep doing it until n is 0 (if n is 4, next time n will be 0 as 4 / 10 is 0)
	while(n > 0){
		// get the digit
		int digit = n % 10;

		// get the square of the digit
		int digitSq = digit * digit;

		// reduce the no - remove the digit from the number
		// 49 / 10 is 4
		n = n / 10;

		sum += digitSq;
	}

	return sum;
}

// Keep getting the sum of square of digits of the number until
// Either the sum hits 1 or the sum hits a number that has already been encountered
bool isHappyNumberHelper(int n, Mymap &m){
	int sum = sumOfSquareOfDigits(n);

	if(sum == 1)
		return true;
	else{
		// check if the number has already been apeared
		Mymap::iterator iter;
		iter = m.find(sum);

		if(iter != m.end()) // number was already encountered
			return false;
		else{
			// it wasn't encountered before
			// insert the number in the hash
			m.insert(Mymap::value_type(sum,1));
			return isHappyNumberHelper(sum,m);
		}
	}
}

bool isHappyNumber(int n){
	Mymap m;
	return isHappyNumberHelper(n,m);
}

// Total Time: O(n^2)
int main(){
	cout << isHappyNumber(7) << endl;
	cout << isHappyNumber(6) << endl;
	cout << isHappyNumber(10) << endl;
	cout << isHappyNumber(13) << endl;
	cout << isHappyNumber(14) << endl;
}