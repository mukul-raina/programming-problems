#include <iostream>
using namespace std;

void helper(int *a, int l, int h, int &min){

	if(l <= h){
		// check current element
		if(a[l] < min)
			min = a[l];

		// call on the rest of the array
		helper(a,l+1,h,min);
	}
}

int getMin(int *a, int l, int h){
	int min = a[l];

	helper(a,l,h,min);

	return min;
}


int main(){
	int a[] = {10,21,13,4,15,2};
	int size = 6;
	cout << getMin(a,0,size - 1) << endl;
	
	return 0;
}