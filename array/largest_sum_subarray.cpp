// we need maxLeft and maxRight (along with left and right) cos
// lets say we found a seq with max sum 
// then the sum got 0 
// then we started keeping track of left and right again right
// but that we might not necesaarily find the max sum!
// but we still need to keep track of temp left and right 
// so we need maxLeft and maxRight just like maxSum
void largestSumSubarray(int *a, int size){
	if(size == 0)
		return -1;
	else{
		int s = 0; // current sum
		int maxSum = 0; // largest sum so far
		int left, right; // starting and ending index of the largest subarray

		for(int i = 0; i < size; i++){
			// get sum
			s += s + a[i];

			if(s <= 0){
				s = 0;
				left = i + 1; // update the starting + ending index of the max sum subsequence
				right = i + 1;
			}
			else{
				// so there's +ve sum
				// compare this sum with the largest sum so far
				if(s > maxSum){
					maxSum = s;
					maxLeft = left; // put the ending index here
					maxRight = i;
					right = i; // both right and maxRight will be i in this case
				}
			}
		}

		cout << maxSum << '\t' << maxLeft << '\t' << maxRight << endl;
	}
}