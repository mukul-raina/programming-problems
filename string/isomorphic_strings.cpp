#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;
typedef unordered_map<char, int> Mymap;

void stringToHash(string s, Mymap &hash){
	if(s.size() == 0){
		return;
	}
	else{
		Mymap::iterator it;

		// Traverse the string + 
		// check if the char is in hash 
		// if its not, insert with its index
		for(int i = 0; i < s.size(); i++){
			// lookup the value
			it = hash.find(s[i]);

			// insert the char, if it wasn't found in the hash
			if(it == hash.end())
				hash.insert(Mymap::value_type(s[i],i));
		}
	}
}

void getEncoding(string s, Mymap &hash, vector<int> &encodingVector){
	Mymap::iterator it;

	// traverse the string
	for(int i = 0; i < s.size(); i++){
		it = hash.find(s[i]); // lookup the char
		encodingVector.push_back(it->second); // insert the firstSeenIndex of the char
	}
}

bool areEncodingsSame(vector<int> encoding1, vector<int> encoding2){
	if(encoding1.size() != encoding2.size())
		return false;
	else{
		int i = 0;

		// compare each element of encoding
		while(i < encoding1.size()){
			// if found to be not same, return
			if(encoding1[i] != encoding2[i])
				return false;

			i++;
		}

		return true;
	}
}

bool areIsomorphic(string s1, string s2){
	if(s1.size() != s2.size())
		return false;
	else{
		// Declare 2 hashes for both of the strings
		// hash-> <char, firstSeenIndex>
		// will help to get the encoding of a string
		Mymap hash1;
		Mymap hash2;

		// put the strings into the hashes
		stringToHash(s1,hash1);
		stringToHash(s2,hash2);

		// get encodings of both the strings
		vector<int> encoding1;
		vector<int> encoding2;

		getEncoding(s1,hash1,encoding1);
		getEncoding(s2,hash2,encoding2);

		// compare the encodings, if same return true 
		return areEncodingsSame(encoding1,encoding2);
	}
}

int main(){
  string s1 = "hate";
  string s2 = "hell";
  string s3 = "foo";
  string s4 = "app";
  string s5 = "foo";
  string s6 = "bar";


  cout << areIsomorphic(s1,s2) << endl;
  cout << areIsomorphic(s3,s4) << endl;
  cout << areIsomorphic(s5,s6) << endl;
  return 0;
}