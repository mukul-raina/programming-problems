// Time: O(n) space O(n)
bool anagram(string s1, string s2){
  if(s1.size() != s2.size()) // if both of them are different sizes, can't be anagram
    return false;
  else{
    int asciiSet[256] = {0}; // assign all values 0 initially
    // Track # of occurences in the first string
    for(int i = 0; i < s1.size(); i++){
      int v = s1[i];
      asciiSet[v]++;
    }

    // Compare the # of occurences with the 2nd string
    // so we keep decreasing the value of the corresponding characters
    for(int i = 0; i < s2.size(); i++){
      int v = s2[i];
      asciiSet[v]--;
    }

    // All elements of asciiSet should be 0 now if
    // characters in both strings appeared the same number of times
    for(int i = 0; i < 256; i++){
      if(asciiSet[i] > 0)
        return false;
    }
    return true;
  }
}