#include <iostream>
using namespace std;

void runLength(string s){
	if(s.size() == 0)
		return;
	else{
		int index = 0;
		char lastChar = s[0];
		int count = 1;

		for(int i = 1; i < s.size(); i++){
			if(s[i] == lastChar)
				count++;
			else{
				cout << s[i - 1] << count;
				lastChar = s[i];
				count = 1;
			}
		}

		cout << lastChar << count << endl;	
	}
}

int main(){
	runLength("aabccccaaa");
	return 0;
}