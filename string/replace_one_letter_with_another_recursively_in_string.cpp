#include <iostream>
using namespace std;

void replace(char *p, char origChar, char replaceChar){
	// base case
	if(p[0] == '\0')
		return;

	// check the current char
	if(p[0] == origChar)
		p[0] = replaceChar;

	// call it on the rest of the string
	replace(p + 1, origChar, replaceChar);
}

int main(){
	char string[] = {'m', 'u', 'k', 'u', 'l', '\0'};
	char *p = string;
	char origChar = 'u';
	char replaceChar = 'a';

	cout << p << endl;
	replace(p,origChar,replaceChar);
	cout << p << endl;
	return 0;
}