#include <iostream>
using namespace std;

void swap(char &a, char &b){
	char temp = a;
	a = b;
	b = temp;
}

void reverseString(string &s, int l, int h){
	if(l == h)
		return;
	else{
		while(l < h){
			swap(s[l],s[h]);
			l++;
			h--;
		}
	}
}

void reverseOrderOfWords(string &s){
	if(s.size() == 0 || s.size() == 1)
		return;
	else{
		// reverse the whole string first
		reverseString(s,0,s.size() - 1);

		int low = 0;

		for(int i = 0; i < s.size() - 1; i++){
			if(s[i] == ' '){
				reverseString(s,low,i - 1);
				low = i + 1;
			}
		}
	}
}

int main(){
	string s = "i like this program very much";

	cout << s << endl;
	reverseOrderOfWords(s);
	cout << s << endl;

	return 0;
}