// delete a given value node in the DLL
// Case 1: 0 node in list
// Case 2: value not in the list
// Case 3: delete 1st node
// Case 4: delete somewhere in the middle
// Case 5: delete the last node
void DLL::deleteByValue(int v){
	if(head == NULL)
		return;
	else{
		// look for the value
		Node *c = head;
		Node *p = NULL;
		Node *n = NULL;

		while(c != NULL){
			if(c->value == v)
				break;

			p = c;
			c = c->next;
		}

		// if c is NULL value wasn't found in the list
		if(c == NULL)
			return;

		// if its the first node in the list
		if(p == NULL){
			n = c->next; // next node after the value
			head = n;
			// only if there's a next node (what if there's only 1 node in the list) - avoiding seg fault
			if(n != NULL)
				n->prev = NULL;
			delete c;
		}
		// if its somewhere in the middle or the last node
		else{
			n = c->next;
			p->next = n;
			// if n is not null (c was not the last node in the list)
			if(n != NULL)
				n->prev = p;
			delete c;
		}
	}
}