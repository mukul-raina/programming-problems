#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
  void BFSTraversal(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

void deleteTree(Node *n){
  if(n == NULL)
    return; // base case
  else{
    // return left and right subtree first
    deleteTree(n->left);
    deleteTree(n->right);

    // now delete the node itself
    // make sure you set the root = NULL in the main()
    delete(n);
  }
}

int getHeight(Node *n){
  if(n == NULL)
    return 0;
  else{
    int left = getHeight(n->left);
    int right = getHeight(n->right);

    return max(left,right) + 1;
  }
}

void maxWidthHelper(Node *n, int *countArray, int level){
  if(n != NULL){
    countArray[level]++;
    maxWidthHelper(n->left,countArray,level + 1); // populate node count from left subtree
    maxWidthHelper(n->right,countArray,level + 1); // populate node count from right subtree
  }
}

int maxArray(int *a, int size){
  if(size == 0)
    return -1;
  else{
    int max = a[0];
    for(int i = 0; i < size; i++){
      if(a[i] > max)
        max = a[i];
    }

    return max;
  }
}

void initializeAllValuesToZero(int *a, int size){
  if(size == 0)
    return;
  else{
    for(int i = 0; i < size; i++)
      a[i] = 0;
  }
}

void printNodeCount(Node *n){
  int level = 0;
  int height = getHeight(n);

  // setup a count array of size Height 
  // this array stores the count of nodes at each level
  int countArray[height];

  // make all values 0 initially
  initializeAllValuesToZero(countArray, height);

  maxWidthHelper(n,countArray,level);

  // count array holds all the level with their node count
  // assuming our levels start with 0
  for(int i = 0; i < height; i++)
    cout << i << " : " << countArray[i] << endl;
}

int main(){
  BST *tree = new BST();

  tree->insert(20);
  tree->insert(25);
  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(12);
  tree->insert(18);
  tree->insert(19);
  tree->insert(3);
  tree->insert(6);

  printNodeCount(tree->root);

  // always delete the tree
  deleteTree(tree->root);
  tree->root = NULL;
  return 0;
};