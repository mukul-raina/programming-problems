#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
  left = NULL;
  right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  // Make a node
  Node *newNode = new Node(v);

  // Case 1: empty tree
  if(root == NULL)
    root = newNode;

  // Case 2: 1 or more node
  else{
    // Traverse and find parent
    Node *c = root;
    Node *p = NULL;
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    // Now p points to the parent
    if(v < p->value)
      p->left = newNode;
    else
      p->right = newNode;
  } 
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;

  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

void deleteTree(Node *n){
  if(n == NULL)
    return; // base case
  else{
    // return left and right subtree first
    deleteTree(n->left);
    deleteTree(n->right);

    // now delete the node itself
    // make sure you set the root = NULL in the main()
    delete(n);
  }
}

void helper(Node *n, int currentLevel, int requiredlevel){
  if(n == NULL)
    return;
 
  // if you found the right level
  else if(currentLevel == requiredlevel)
    cout << n->value << '\t';

  // otherwise go deeper in left and right subtrees
  // by incrementing currentLevel
  else{
    helper(n->left,currentLevel + 1, requiredlevel);
    helper(n->right,currentLevel + 1, requiredlevel);
  }
}

void printAllLevelNodes(Node *n, int requiredlevel){
  int currentLevel = 0;

  helper(n,currentLevel,requiredlevel);
}

int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(3);
  tree->insert(6);
  tree->insert(11);

  tree->inOrder(tree->root);
  cout << endl;

  printAllLevelNodes(tree->root,2);
  cout << endl;

  // always delete the tree
  deleteTree(tree->root);
  tree->root = NULL;
  return 0;
}