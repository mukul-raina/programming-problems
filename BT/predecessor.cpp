#include <iostream>
using namespace std;

// Node
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    void insert(int v);
    void printInOrder(Node *node);
    int predecessor(int v);
    int getMax(Node *n);
    int getMin(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(root == NULL){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

int BST::getMax(Node *n){
  if(n->right == NULL)
    return n->value;
  else
    return getMax(n->right);
}

int BST::getMin(Node *n){
  if(n->left == NULL)
    return n->value;
  else
    return getMin(n->left);
}

bool singleNodeTree(Node *n){
  return (n->left == NULL && n->right == NULL);
}

Node* exists(Node *n, int v){
  if(n == NULL)
    return NULL;
  else{
    Node *c = n;
    while(c != NULL){
      if(c->value == v)
        return c;
      else if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }
    // if loop came till here that means the value wasnt found
    return c;
  }
}


int BST::predecessor(int v){
  if(root == NULL || singleNodeTree(root))
    return -1;

  Node *c = exists(root,v);

  if(c == NULL)
    return -1;

  if(v == getMin(root))
    return -1;

  if(c->left != NULL)
    return getMax(c->left);

  Node *pred = NULL;
  c = root;
  while(c->value != v){
    if(v > c->value){
      pred = c;
      c = c->right;
    }
    else 
      c = c->left;
  }

  return pred->value;
}


int main(){
  BST *tree = new BST();
  tree->insert(8);
  tree->insert(5);
  tree->insert(10);
  tree->insert(4);
  tree->insert(7);
  tree->insert(9);
  tree->insert(14);
  tree->insert(3);
  tree->printInOrder(tree->root);
  cout << endl;
  
  std::cout<<"The predecessor of 99 is: "<<tree->predecessor(99)<<'\n';
  std::cout<<"The predecessor of 8 is: "<<tree->predecessor(8)<<'\n';
  std::cout<<"The predecessor of 10 is: "<<tree->predecessor(10)<<'\n';
  std::cout<<"The predecessor of 14 is: "<<tree->predecessor(14)<<'\n';
  return 0;
}