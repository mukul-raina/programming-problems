#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    // this will also put duplicate values on the right side
    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

void deleteTree(Node *n){
  if(n == NULL)
    return; // base case
  else{
    // return left and right subtree first
    deleteTree(n->left);
    deleteTree(n->right);

    // now delete the node itself
    // make sure you set the root = NULL in the main()
    delete(n);
  }
}

void helper(Node *n, int prev, int &count){
  if(n == NULL)
    return;
  else{
    // get the counts from the left subtree
    helper(n->left, prev, count);

    // check the current node
    if(n->value == prev)
      count++;
    else
      prev = n->value;

    // get the counts from the right subtree
    helper(n->right, prev, count);
  }
}

int getNumOfDuplicates(Node *n){
  int prev = -1; // generally INT_MIN (-1 will work for now)
  int count = 0;

  helper(n, prev, count);

  return count;
}

int main(){
  BST *tree = new BST();

  tree->insert(5);
  tree->insert(3);
  tree->insert(6);
  tree->insert(6);
  tree->insert(2);
  tree->insert(4);
  tree->insert(2);

  tree->inOrder(tree->root);
  cout << endl;

  cout << getNumOfDuplicates(tree->root) << endl;

  // always delete the tree
  deleteTree(tree->root);
  tree->root = NULL;
  return 0;
};