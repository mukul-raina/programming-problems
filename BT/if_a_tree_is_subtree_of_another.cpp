bool identicalTrees(Node *a, Node *b){
	// both of them are null
	if(a == NULL && b == NULL)
		return true;

	// both of them are not null
	else if(a != NULL && b != NULL)
		return ((a->value == b->value) && identicalTrees(a->left,b->left) && identicalTrees(a->right,b->right));
	
	// 1 is NULL wherease 1 is not
	else 
		return false;
}

// check if b is subtree of a
bool isSubtree(Node *a, Node *b){
	// search 'a' tree to see if b is there, if there is get a pointer to it (do BFS for a binary tree)
	Node* p = getPointer(a,b);

	// now p points to the 'b' node inside the 'a' tree
	// if 'p' and 'b' turn out to be identical trees, that would mean b is a subtree of a

	return identicalTrees(p,b);
}