#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

void deleteTree(Node *n){
  if(n == NULL)
    return; // base case
  else{
    // return left and right subtree first
    deleteTree(n->left);
    deleteTree(n->right);

    // now delete the node itself
    // make sure you set the root = NULL in the main()
    delete(n);
  }
}

/*
  - We actually don't need getPointerToNode()
  - We can just calculate the distance + when checking for the node
  - Basically combine both the functions into one
*/

int distanceHelper(Node *n, int v, int distance){
  if(n == NULL)
    return -1;
  else{
    if(n->value == v)
    return distance;

    // check left subtree
    int leftDistance = distanceHelper(n->left, v, distance + 1);

    // if distance found in left subtree, return from here only
    if(leftDistance != -1)
      return leftDistance;

    // check the right subtree
    int rightDistance = distanceHelper(n->right, v, distance + 1);
    return rightDistance; // no other option now, whatever the result right gives, return that 
  }
}

int distanceFromRoot(Node *n, int v){
  int distance = 0;
  return distanceHelper(n,v,distance);
}

int main(){
  BST *tree = new BST();

  tree->insert(20);
  tree->insert(25);
  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(12);
  tree->insert(18);
  tree->insert(19);
  tree->insert(3);
  tree->insert(6);

  int value = 3;
  cout << distanceFromRoot(tree->root, value) << endl;

  // always delete the tree
  deleteTree(tree->root);
  tree->root = NULL;
  return 0;
};