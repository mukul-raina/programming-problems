Node* BST::makeBST(int *a, int low, int high){
  // no elements left - base case
  if(low > high)
    return NULL;

  else{
  	// get mid
    int mid = (low + high) / 2;
    
    // make a node
    Node *n = new Node(a[mid]);

    // generate left side of subtree and get its root
    n->left = makeBST(a,low,mid - 1);

    // generate right side of subtree
    n->right = makeBST(a,mid + 1,high);

    // return the root for the bigger tree
    return n;
  }
}