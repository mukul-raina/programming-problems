#include <iostream>
using namespace std;

// Node
class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
  Node();
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void inOrderTraversal(Node *node);
  void insert(int v);
  Node* sortedArrayToBST(int *a, int l, int h);
};

void BST::insert(int v){
  // Make a new node
  Node *newNode = new Node(v);

  // Case 1: empty tree
  if(!root){
    root = newNode;
    return;
  }

  // Case 2: Normal case
  // Traverse through the tree + find the parent
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;

    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

BST::BST(){
  root = NULL;
}

void BST::inOrderTraversal(Node *node){
  if(node != NULL){
    inOrderTraversal(node->left);
    cout << node->value << '\t';
    inOrderTraversal(node->right);
  }
}

Node* sortedArrayToBST(int *a, int l, int h){
  if(l > h)
    return NULL;
  else{
    // get middle
    int m = (l + h) / 2;

    // make the root have the mid value
    Node *n = new Node(a[m]);

    // recursively call the function on the left subtree
    n->left = sortedArrayToBST(a,l,m-1);

    // call on right subarray
    n->right = sortedArrayToBST(a,m+1,h);

    // return the root
    return n;
  }
}

int main(){
  int a[] = {10,15,17,20,25,30,32};
  int size = 7;

  BST *tree = new BST();

  tree->root = sortedArrayToBST(a,0,size-1);

  tree->inOrderTraversal(tree->root);
  cout << endl;

  return 0;
}