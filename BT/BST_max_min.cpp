#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
  left = NULL;
  right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  // Make a node
  Node *newNode = new Node(v);

  // Case 1: empty tree
  if(root == NULL)
    root = newNode;

  // Case 2: 1 or more node
  else{
    // Traverse and find parent
    Node *c = root;
    Node *p = NULL;
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    // Now p points to the parent
    if(v < p->value)
      p->left = newNode;
    else
      p->right = newNode;
  } 
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;

  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

void deleteTree(Node *n){
  if(n == NULL)
    return; // base case
  else{
    // return left and right subtree first
    deleteTree(n->left);
    deleteTree(n->right);

    // now delete the node itself
    // make sure you set the root = NULL in the main()
    delete(n);
  }
}

// Time: O(n) Space: O(1)
int getMinIterative(Node *n){
  if(n == NULL)
    return -1;
  else{
    Node *c = n;

    // iterate to the smallest node
    while(c->left != NULL)
      c = c->left;

    return c->value;
  }
}

// Time: O(n) Space: O(n)
int getMin(Node *n){
  if(n == NULL) // empty tree
    return -1;
  else if(n->left == NULL) // base case
    return n->value;
  else
    return getMin(n->left); // min of the left subtree
}

int getMaxIterative(Node *n){
  if(n == NULL)
    return -1;
  else{
    // iterate to the biggest node
    Node *c = n;

    while(c->right != NULL)
      c = c->right;

    return c->value;
  }
}

int getMax(Node *n){
  if(n == NULL)
    return -1;
  else if(n->right == NULL)
    return n->value;
  else
    return getMax(n->right);
}

int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(3);
  tree->insert(6);
  tree->insert(11);

  tree->inOrder(tree->root);
  cout << endl;

  // test cases
  cout << getMax(tree->root) << endl;
  cout << getMin(tree->root) << endl;
  cout << getMinIterative(tree->root) << endl;
  cout << getMaxIterative(tree->root) << endl;


  // always delete the tree
  deleteTree(tree->root);
  tree->root = NULL;
  return 0;
};