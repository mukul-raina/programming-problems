#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
  void BFSTraversal(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

// Returns the sum of all nodes of a tree
int sumOfAllNodes(Node* n){
  if(n == NULL)
    return 0;
  else{
    int l = sumOfAllNodes(n->left);
    int r = sumOfAllNodes(n->right); 
    
    return (n->value + l + r);
  }
}

// Time: O(n^2)
void sumTree1(Node *n){
  queue<Node *> q;

  q.push(n);

  while(q.empty() != true){
    Node *t = q.front();
    q.pop();

    // so the value of the node will be the sum of left subtree + sum of right subtree
    // sumOfAllNodes takes O(n), so you call O(n) + O(n) -> O(n) for 1 node
    // when we do it for all nodes it will be O(n^2)
    t->value = sumOfAllNodes(t->left) + sumOfAllNodes(t->right);

    // push if there's a left child
    if(t->left != NULL)
      q.push(t->left);

    // push if there's a right child
    if(t->right != NULL)
      q.push(t->right);
  }
}

int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(3);
  tree->insert(6);
  tree->insert(12);
  tree->insert(18);

  tree->inOrder(tree->root);
  cout << endl;

  sumTree1(tree->root);

  tree->inOrder(tree->root);
  cout << endl;
  
  return 0;
};