#include <iostream>
using namespace std;

// Node
class Node{
  public:
    int value;
    Node *left;
    Node *right;
    Node();
    Node(int v);
};
Node::Node(){
  left = right = NULL;
}
Node::Node(int v){
  value = v;
  left = right = NULL;
}

// Tree
class BST{
  public:
    Node *root;
    BST();
    void insert(int v);
    void printInOrder(Node *node);
    int successor(int v);
    int getMax(Node *n);
    int getMin(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);
  
  // Case 1: If empty tree
  if(root == NULL){
    root = newNode;
    return;
  }

  // Case 2: Find parent (by traversing the tree)
  Node *parent = NULL;
  Node *current = root;
  while(current != NULL){
    parent = current;
    if(v < current->value)
      current = current->left;
    else
      current = current->right;
  }

  // Now parent points to the parent 
  if(v < parent->value)
    parent->left = newNode;
  else
    parent->right = newNode;
}

void BST::printInOrder(Node *node){
  if(node != NULL){
    printInOrder(node->left);
    cout << node->value << '\t';
    printInOrder(node->right);
  }
}

int BST::getMax(Node *n){
  if(n->right == NULL)
    return n->value;
  else
    return getMax(n->right);
}

int BST::getMin(Node *n){
  if(n->left == NULL)
    return n->value;
  else
    return getMin(n->left);
}

// print all the nodes that don't have siblings
// basically who are the only child of their parent
void printSingleChild(Node *n){
	if(n == NULL)
		return;
	else{
		// Check n's left child if he's singleChild
		if(n->left != NULL && n->right == NULL)
			cout << n->left->value << endl;

		// Check n's right child if he's singleChild
		if(n->right != NULL && n->left == NULL)
			cout << n->right->value << endl;

		// Do the same checks as above in both the right + left subtree
		printSingleChild(n->left);
		printSingleChild(n->right);
	}
}

int main(){
  BST *tree = new BST();
  tree->insert(8);
  tree->insert(5);
  tree->insert(10);
  tree->insert(4);
  tree->insert(7);
  tree->insert(9);
  tree->insert(3);
  tree->printInOrder(tree->root);
  cout << endl;
  
  printSingleChild(tree->root);
  return 0;
}