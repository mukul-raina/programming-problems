#include <iostream>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
  left = NULL;
  right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  // Make a node
  Node *newNode = new Node(v);

  // Case 1: empty tree
  if(root == NULL)
    root = newNode;

  // Case 2: 1 or more node
  else{
    // Traverse and find parent
    Node *c = root;
    Node *p = NULL;
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    // Now p points to the parent
    if(v < p->value)
      p->left = newNode;
    else
      p->right = newNode;
  } 
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;

  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

void deleteTree(Node *n){
  if(n == NULL)
    return; // base case
  else{
    // return left and right subtree first
    deleteTree(n->left);
    deleteTree(n->right);

    // now delete the node itself
    // make sure you set the root = NULL in the main()
    delete(n);
  }
}

int BST::successor(int v){
  // Case 1: if tree is empty
  if(root == NULL)
    return -1;
  else{
    // traverse to the node (check if its present in the tree or not)
    Node *c = root;
    
    while(c != NULL){
      if(c->value == v)
        break;
      else if(v < c->value)
        c = c->left;
      else 
        c = c->right;
    }

    // Case 2: if c is NULL, that means node wasn't found in the tree
    if(c == NULL)
      return -1;

    // Case 3: Check if the node is max value in the tree
    // cos if its the max value then there can't be any successor
    // Plus now c must be pointing to the node (for which we have to find the successor)
    if(c->value == getMax(root))
      return -1;

    // Case 4: Now check if it has a right subtree or not
    // cos if it has a right subtree then we just return the min of the right subtree (That'll be our successor)
    if(c->right != NULL)
      return getMin(c->right);

    // Case 5: Node doesn't have a right subtree
    // So we traverse from the start again and have "succ" pointer that keeps track of successor
    Node *succ = NULL;
    c = root;

    // we can have this while condition cos we know by now that node is present in the tree
    while(c->value != v){
      if(v > c->value)
        c = c->right;
      else{
        succ = c;
        c = c->left;
      }
    }

    return succ->value;
  }
}
int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(3);
  tree->insert(6);
  tree->insert(11);

  tree->inOrder(tree->root);
  cout << endl;

  // always delete the tree
  deleteTree(tree->root);
  tree->root = NULL;
  return 0;
};