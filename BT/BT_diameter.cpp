#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
  void BFSTraversal(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

void deleteTree(Node *n){
  if(n == NULL)
    return; // base case
  else{
    // return left and right subtree first
    deleteTree(n->left);
    deleteTree(n->right);

    // now delete the node itself
    // make sure you set the root = NULL in the main()
    delete(n);
  }
}

// Time: O(n)
/*
  NOTE: diameter of a tree is NOT (height of Left + height of Right + 1)
  BUT it is maximum of the following values:
    - diameter of the left subtree
    - diameter of the right subtree
    - (height of Left + height of Right + 1)
      - why?
        - cos essentially the height is the longest distance from root to a leave
        - so basically height of left subtree gives you longest distance of root to leave in left subtree
        - height of right gives you the longest distance in the right subtree
        - now if you add those 2 values + 1 + compare with the diameter of left + right subtree
        - the comparison (which is essentially finding the maximum) will give you the result
*/
int diameterHelper(Node *n, int &height){
  if(n == NULL){
    height = 0;
    return 0; // diameter is 0 as well
  }
  else{

    int rightHeight = 0;
    int leftHeight = 0;

    int leftDiameter = diameterHelper(n->left,leftHeight);
    int rightDiameter = diameterHelper(n->right,rightHeight);

    height = max(leftHeight,rightHeight) + 1; // update the height of the curren tree

    return max(leftHeight + rightHeight + 1, max(leftDiameter,rightDiameter));
  }
}

int diameter(Node *n){
  int height = 0;

  int diameter = diameterHelper(n,height);

  return diameter;
}

int main(){
  BST *tree = new BST();

  tree->insert(20);
  tree->insert(25);
  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(12);
  tree->insert(18);
  tree->insert(19);
  tree->insert(3);
  tree->insert(6);

  cout << diameter(tree->root) << endl;

  // always delete the tree
  deleteTree(tree->root);
  tree->root = NULL;
  return 0;
};