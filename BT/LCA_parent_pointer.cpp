int distanceHelper(Node *n, int v, int distance){
  if(n == NULL)
    return -1;
  else{
    if(n->value == v)
    return distance;

    // check left subtree
    int leftDistance = distanceHelper(n->left, v, distance + 1);

    // if distance found in left subtree, return from here only
    if(leftDistance != -1)
      return leftDistance;

    // check the right subtree
    int rightDistance = distanceHelper(n->right, v, distance + 1);
    return rightDistance; // no other option now, whatever the result right gives, return that 
  }
}

int distanceFromRoot(Node *n, int v){
  int distance = 0;
  return distanceHelper(n,v,distance);
}

Node *getKthNodeFromFront(Node *p, int k){
	// This is a bit diff for a tree (as compared to LL)
	// cos in this we traverse the ->parent pointer insted of ->next
	if(p == NULL || k == 0)
		return NULL;
	else{
		int i = 1;
		Node *c = p;

		while(c != NULL){
			if(i == k)
				break;

			c = c->parent;
			i++;
		}

		if(c == NULL)
			return NULL;
		else
			return c;
	}
}

Node *getLCAHelper(Node *p, Node *q, int diff){
	// IMP: so we want to move the longer path pointer (p) to 
	// (diff + 1) position, so that both p and q will be at the same level
	// then we just increment both of them at the same rate (by 1)
	// this question is pretty similar to Y shapred LL


	// so first lets increment p to (diff + 1) node
	// this is basically same as the LL function -> getKthNodeFromFront
	Node *kThNode = getKthNodeFromFront(p,diff + 1);

	// now increment both nodes at the same rate
	while(kThNode != NULL && q != NULL){
		if(kThNode == q)
			return q; // bascially if at any point they become equal (hence meet) - that node is their LCA

		kThNode = kThNode->parent;
		q = q->parent;
	}

	// if reached till here, not found
	return NULL;
}

Node *getLCA(Node *, Node *a, Node *b){
	// if tree is empty
	if(n == NULL)
		return NULL;
	
	// get the distance of a and b from root - O(n)
	int lengthA = distanceFromRoot(n,a); 
	int lengthB = distanceFromRoot(n,b);

	// if any of those 2 nodes are not in the tree
	if(lengthA == -1 || lengthB == -1)
		return NULL;
	
	if(a > b){
		int diff = a - b;
		return getLCAHelper(a,b,diff); // the first node will be the longer path one
	}
	else{
		int diff = b - a;
		return getLCAHelper(b,a,diff); // the first node will be the longer path one
	}
}