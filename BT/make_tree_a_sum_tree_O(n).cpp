#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
  void BFSTraversal(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

// Time: O(n) cos we access each node just once
int makeSumTree(Node* n){
  if(n == NULL)
    return 0;
  else{
    int l = makeSumTree(n->left);
    int r = makeSumTree(n->right); 
    
    int oldNodeValue = n->value;
    n->value = l + r; // we replace the value of node with the sum of its left + right subtrees

    return oldNodeValue + l + r; // but we return the old value of the node + sum of left and right subtrees
  }
}

int main(){
  BST *tree = new BST();

  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(3);
  tree->insert(6);
  tree->insert(12);
  tree->insert(18);

  cout << makeSumTree(tree->root) << endl;

  tree->inOrder(tree->root);
  cout << endl;
  return 0;
};