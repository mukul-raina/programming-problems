#include <iostream>
#include <queue>
using namespace std;

class Node{
public:
  int value;
  Node *left;
  Node *right;
  Node(int v);
};
Node::Node(int v){
  this->value = v;
  left = right = NULL;
}

class BST{
public:
  Node *root;
  BST();
  void insert(int v);
  void inOrder(Node *n);
};
BST::BST(){
  root = NULL;
}
void BST::insert(int v){
  Node *newNode = new Node(v);

  if(root == NULL)
    root = newNode;
  else{
    Node *c = root;
    Node *p = NULL;
    
    while(c != NULL){
      p = c;
      if(v < c->value)
        c = c->left;
      else
        c = c->right;
    }

    if(v < p->value)
      p->left = newNode;
    else 
      p->right = newNode;
  }
}

void BST::inOrder(Node *n){
  if(n == NULL)
    return;
  else{
    inOrder(n->left);
    cout << n->value << '\t';
    inOrder(n->right);
  }
}

void deleteTree(Node *n){
  if(n == NULL)
    return; // base case
  else{
    // return left and right subtree first
    deleteTree(n->left);
    deleteTree(n->right);

    // now delete the node itself
    // make sure you set the root = NULL in the main()
    delete(n);
  }
}

int helper(Node *n, int currentLevel, int givenLevel){
  if(n == NULL)
    return 0;

  else if(currentLevel == givenLevel)
    return n->value;

  else{
    // get the level sum from the left subtree
    int left = helper(n->left, currentLevel + 1, givenLevel);

    // get the level sum from the right subtree
    int right = helper(n->right, currentLevel + 1, givenLevel);

    return left + right;
  }
}

int getLevelSum(Node *n, int givenLevel){
  int currentLevel = 0;
  return helper(n,currentLevel,givenLevel);
}

int main(){
  BST *tree = new BST();

  tree->insert(20);
  tree->insert(25);
  tree->insert(10);
  tree->insert(5);
  tree->insert(15);
  tree->insert(12);
  tree->insert(18);
  tree->insert(19);
  tree->insert(3);
  tree->insert(6);

  int level = 3;

  cout << getLevelSum(tree->root, level) << endl;

  // always delete the tree
  deleteTree(tree->root);
  tree->root = NULL;
  return 0;
};