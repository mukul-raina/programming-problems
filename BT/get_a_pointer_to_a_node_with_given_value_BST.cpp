/*
 * @param	r	Points to the root of the tree 
 * @param v We need to return a pointer poiniting a node having this value
 * @return	Returns a pointer to a node with value v otherwise if not found we return NULL
 */
Node *getPointerBST(Node *r, int v){
	// check if tree is empty
	if(r == NULL)
		return NULL;

	else{
		Node *c = r;
		while(c != NULL){
			if(c->value == v)
				break;
			else if(v < c->value)
				c = c->left;
			else
				c = c->right;
		}

		// if c points to NULL the value wasn't found 
		// If c is not NULL then its pointing to the node so return that
		if(c == NULL)
			return NULL;
		else
			return c;
		}
}